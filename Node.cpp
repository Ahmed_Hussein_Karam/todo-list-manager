#ifndef NODE_CPP
#define NODE_CPP
#include "Node.h"

template <typename T>
Node<T>::Node()
{
    next = prev = NULL; //count = 0;
}

template <typename T>
Node<T>::Node(T dataItem, Node<T>* nextPtr, Node<T>* prevPtr)
{
    data = dataItem; next = nextPtr; prev = prevPtr;
}

template <typename T>
Node<T>* Node<T>::getNext() const
{
    return next;
}

template <typename T>
Node<T>* Node<T>::getPrev() const
{
    return prev;
}

template <typename T>
T Node<T>::getData() const
{
    return data;
}

template <typename T>
void Node<T>::setNext(Node<T>* ptr)
{
    next = ptr;
}

template <typename T>
void Node<T>::setPrev(Node<T>* ptr)
{
    prev = ptr;
}

template <typename T>
void Node<T>::setData(T dataItem)
{
    data = dataItem;
}

template <typename T>
ostream& operator<< (ostream& out, Node<T> node)
{
    out << node.getData();
    return out;
}

#endif
