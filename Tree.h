#ifndef TREE_H
#define TREE_H
#include "TreeNode.h"

template <typename T>
class Tree
{
protected:
    TreeNode<T>* root;
public:
    TreeNode<T>* getRoot() const;
    void setRoot(TreeNode<T>* ptr);
    virtual void insert(T item)=0;
    virtual void remove(TreeNode<T>* treeNode)=0;
    int size(TreeNode<T> *Root) const;
};
#include "Tree.cpp"
#endif // TREE_H
