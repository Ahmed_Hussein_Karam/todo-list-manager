#ifndef AVL_CPP
#define AVL_CPP
#include "AVL.h"

template <typename T>
AVL<T>::AVL(bool saveWhenDestruc):BST<T>(false),saveWhenDestructed(saveWhenDestruc)
{
    this->root=NULL;
    this->loadFile();
}

template <typename T>
void AVL<T>::insert(T item)
{
    int number_comp=0;
    TreeNode<T>* temp=this->root;
    TreeNode<T>* newNode=new TreeNode<T>{};
    newNode->setData(item);
    if(this->root==NULL)
    {
        number_comp++;
        this->root=newNode;
        cout<<"\n\tNumber of comparisons = "<<number_comp<<'\n';
        return;
    }
    stack<pair<TreeNode<T>*,bool> > stk;
    while(true)
    {
        number_comp++;
        if(strcmp(newNode->getData().getName(),temp->getData().getName())> 0)
        {
            stk.push(make_pair(temp,0));
            if(temp->getRight()==NULL) {temp->setRight(newNode); break;}
            else temp=temp->getRight();
        }
        else
        {
            stk.push(make_pair(temp,1));
            if(temp->getLeft()==NULL) {temp->setLeft(newNode); break;}
            else temp=temp->getLeft();
        }
    }
    temp=newNode;
    while(!stk.empty())
    {
        number_comp++;
        bool dir=stk.top().second;
        if(dir==0)
            stk.top().first->setRight(balance(temp));
        else
            stk.top().first->setLeft(balance(temp));
        temp=stk.top().first;
        if(stk.size()==1)
            this->root=balance(stk.top().first);
        stk.pop();
    }
    cout<<"\n\tNumber of comparisons = "<<number_comp<<'\n';
}

template <typename T>
void AVL<T>::remove(TreeNode<T>* target)
{
    static int number_comp=0;
    if(target==NULL)
    {
        cout<<"\n\tNumber of comparisons = "<<number_comp<<"\t[can not remove null ]"<<'\n'; //not insert
        return;
    }
    TreeNode<T>* temp=this->root;
    TreeNode<T>* parent=NULL;
    stack<pair<TreeNode<T>*,bool> > stk;
    while(temp!=target && temp!=NULL)
    {
        number_comp++;
        parent=temp;
        if(strcmp(target->getData().getName(),temp->getData().getName()) > 0)
        {
            stk.push(make_pair(temp,0));
            temp=temp->getRight();
        }
        else
        {
            stk.push(make_pair(temp,1));
            temp=temp->getLeft();
        }
    }
    if(temp==NULL)
    {
        cout<<"\n\tNumber of comparisons = "<<number_comp<<"\t[ not found ]"<<'\n';
        return;
    }
    else if(temp->getLeft()==NULL && temp->getRight()==NULL)//No children
    {
        if(parent!=NULL)
        {
            if(temp==parent->getLeft())
                parent->setLeft(NULL);
            else
                parent->setRight(NULL);

        }
        else
            this->root=NULL;
        delete temp;
        temp=NULL;
    }
    else if(temp->getLeft()==NULL || temp->getRight()==NULL)//only one child
    {
        if(parent!=NULL)
        {
            if(temp==parent->getLeft())
            {
                if(temp->getLeft()!=NULL)
                    parent->setLeft(temp->getLeft());
                else
                    parent->setLeft(temp->getRight());
                delete temp;
                temp=parent->getLeft();
            }
            else
            {
                if(temp->getLeft()!=NULL)
                    parent->setRight(temp->getLeft());
                else
                    parent->setRight(temp->getRight());
                delete temp;
                temp=parent->getRight();
            }
        }
        else
        {
            if(temp->getLeft()!=NULL)
                this->root=temp->getLeft();
            else
                this->root=temp->getRight();
            delete temp;
            cout<<"\n\tNumber of comparisons = "<<number_comp<<'\n';
            return;
        }

    }
    else//two children
    {
        TreeNode<T>* maxLeft=temp->getLeft();
        while(maxLeft->getRight()!=NULL)
        {
            number_comp++;
            maxLeft=maxLeft->getRight();
        }
        temp->setData(maxLeft->getData());
        remove(maxLeft);
        return;
    }
    while(!stk.empty())
    {
        number_comp++;
        bool dir=stk.top().second;
        if(dir==0)
            stk.top().first->setRight(balance(temp));
        else
            stk.top().first->setLeft(balance(temp));
        temp=stk.top().first;
        if(stk.size()==1)
            this->root=balance(stk.top().first);
        stk.pop();
    }
    cout<<"\n\tNumber of comparisons = "<<number_comp<<'\n';
}

template <typename T>
TreeNode<T>* AVL<T>::balance(TreeNode<T> *node)
{
    if(node!=NULL)
    {
        int balanceFac=balanceFactor(node);
        if(abs(balanceFac)>1)
        {
            int leftBalance=balanceFactor(node->getLeft());
            int rightBalance=balanceFactor(node->getRight());
            if(balanceFac>0)
            {
                if(leftBalance>=0)//left left
                    return rotateRight(node);
                else//left right
                {
                    node->setLeft(rotateLeft(node->getLeft()));
                    return rotateRight(node);
                }
            }
            else
            {
                if(rightBalance<=0)//right right
                    return rotateLeft(node);
                else//right left
                {
                    node->setRight(rotateRight(node->getRight()));
                    return rotateLeft(node);
                }
            }
        }
        return node;
    }
    return NULL;
}

template <typename T>
int AVL<T>::balanceFactor(TreeNode<T> *node)
{
    if(node!=NULL)
        return height(node->getLeft())-height(node->getRight());
    return 0;
}

template <typename T>
int AVL<T>::height(TreeNode<T> *node)
{
    if(node!=NULL)
    {
        int rightHeight=0,leftHeight=0;
        if(node->getRight()!=NULL)
            rightHeight=1+height(node->getRight());
        if(node->getLeft()!=NULL)
            leftHeight=1+height(node->getLeft());
        return max(rightHeight,leftHeight);
    }
    return -1;
}

template <typename T>
TreeNode<T>* AVL<T>::rotateRight(TreeNode<T> *node)
{
    if(node!=NULL && node->getLeft()!=NULL)
    {
        TreeNode<T> *temp=node->getLeft();
        node->setLeft(temp->getRight());
        temp->setRight(node);
        return temp;
    }
    return node;
}

template <typename T>
TreeNode<T>* AVL<T>::rotateLeft(TreeNode<T> *node)
{
    if(node!=NULL && node->getRight()!=NULL)
    {
        TreeNode<T> *temp=node->getRight();
        node->setRight(temp->getLeft());
        temp->setLeft(node);
        return temp;
    }
    return node;
}

template <typename T>
AVL<T>::~AVL()
{
    if(saveWhenDestructed==true)
        this->save();
    this->clear(this->root);
}
#endif // AVL_CPP
