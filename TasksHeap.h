#ifndef TASKSHEAP_H
#define TASKSHEAP_H

#include <iostream>
#include <vector>
#include <string>
#include <cstring>
#include <fstream>
#include "Task.h"
#include "Tree.h"

using namespace std;

template <typename T>
class TasksHeap
{
private:
    T* heapElements;   // Pointer to array of heap items
    int SizeOfArray;
    const int MaxSize=500;
    void LoadFile();
protected:
    void ShiftUp(int Node,int&x);      // Shift Node up into place
	void ShiftDown(int Node,int&x);    // Shift Node down into place
    int ParentOf(int Node);      // Returns Parent location
    int LeftChildOf(int Node);   // Returns Left Child location
    bool is_empty(std::ifstream& pFile);
public:
    TasksHeap ();      // Create empty heap
    TasksHeap (T* heapElement, int siz);
    void insert(T elem);  // Add item at end & sift-up
    T  removeRoot();  // Return top item & reheap
    T search (string taskName);// find task by name
    T*   heapSort();  // Return items sorted by priority
    int size();
    void save();
    ~TasksHeap ();
    void Display();
};

#include "TasksHeap.cpp"
#endif

