#ifndef TREE_NODE
#define TREE_NODE
#include <iostream>

using namespace std;

template <typename T>
class TreeNode
{
private:
    T data;
    TreeNode<T>* left;
    TreeNode<T>* right;
public:
    TreeNode();
    TreeNode(T dataItem,TreeNode<T>* leftPtr=NULL,TreeNode<T>* rightPtr=NULL);

    TreeNode<T>* getLeft() const;
    TreeNode<T>* getRight() const;
    T getData() const;
    void setLeft(TreeNode<T>* ptr);
    void setRight(TreeNode<T>* ptr);
    void setData(T dataItem);

    friend ostream& operator<<(ostream& out, TreeNode<T> treeNode);
};

#include "TreeNode.cpp"
#endif // TREE_NODE
