#ifndef TREE_NODE_CPP
#define TREE_NODE_CPP
#include "TreeNode.h"
template <typename T>
TreeNode<T>::TreeNode()
{
    left=right=NULL;
}

template <typename T>
TreeNode<T>::TreeNode(T dataItem,TreeNode<T>* leftPtr,TreeNode<T>* rightPtr)
{
    data = dataItem; left = leftPtr; right = rightPtr;
}

template <typename T>
TreeNode<T>* TreeNode<T>::getLeft() const
{
    return left;
}

template <typename T>
TreeNode<T>* TreeNode<T>::getRight() const
{
    return right;
}

template <typename T>
T TreeNode<T>::getData() const
{
    return data;
}

template <typename T>
void TreeNode<T>::setLeft(TreeNode<T>* ptr)
{
    left=ptr;
}

template <typename T>
void TreeNode<T>::setRight(TreeNode<T>* ptr)
{
    right=ptr;
}

template <typename T>
void TreeNode<T>::setData(T dataItem)
{
    data=dataItem;
}

template <typename T>
ostream& operator<<(ostream& out,TreeNode<T> treeNode)
{
    out<<treeNode.getData();
    return out;
}
#endif // TREE_NODE_CPP
