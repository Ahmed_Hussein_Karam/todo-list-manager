#ifndef TASKS_LIST_CPP
#define TASKS_LIST_CPP
#include "TasksList.h"

template < typename T >
TasksList<T>::TasksList()
{
    double tempPriority; //Used to setPriority()
    int tempDuration; //Used to setNumberOfDays()
	head = tail = NULL;
	ifstream file;
	file.open("MyTasks.txt", ios::in);
	Task R;
	if (!is_empty(file))
	{
		file.seekg(0);
		int x;
		file >> x;
		int i = 0;
		while (i<x/*!file.eof()*/)
		{
		    file.ignore();
			file.getline(R.getName(), 100, '\n');
			file.getline(R.getDescription(), 100, '\n');
			file >> tempPriority;
			R.setPriority(tempPriority);
			file.ignore();
			file.getline(R.getDueDate(), 11, '\n');
			file>>tempDuration;
			R.setNumberOfDays(tempDuration);
			addInOrder(R);
			i++;
		}
	}
	file.close();
}

template < typename T >
Node<T>* TasksList<T>::getHead() const
{
	return head;
}

template < typename T >
Node<T>* TasksList<T>::getTail() const
{
	return tail;
}

template < typename T >
void TasksList<T>::setHead(Node<T>* ptr)
{
	head = ptr;
}

template < typename T >
void TasksList<T>::setTail(Node<T>* ptr)
{
	tail = ptr;
}

template < typename T >
void TasksList<T>::addToHead(T item)
{
	Node<T>* ptr = new Node<T>(item);
	if (tail == NULL)
		tail = head = ptr;
	else
    {
        Node<T> *curr = head;
        curr->setPrev(ptr);
        ptr->setNext(curr);
        head = ptr;
    }
}

template < typename T >
void TasksList<T>::addToTail(T item)
{
	Node<T>* ptr = new Node<T>(item);
	if (head == NULL)
		head = tail = ptr;
	else
	{
		Node<T> *curr = tail;
		curr->setNext(ptr);
		ptr->setPrev(curr);
		tail = ptr;
	}
}

template <class T>
void TasksList<T>::addToIndex(T item, int index) {
	Node<T>* nPtr = new Node<T>(item, NULL);
	Node<T>* pPtr = head;
	for (int i = 0; pPtr != 0 && i < index - 1; i++)
		pPtr = pPtr->getNext();
	if (pPtr != 0 && !(index < 0))
    {
        if (index == 0)
			addToHead(item);
		else {
			nPtr->setNext(pPtr->getNext());
			pPtr->setNext(nPtr);
			if (tail == pPtr)
                tail = tail->getNext();
		}
    }
}

template < typename T >
void TasksList<T>::addInOrder(T item)
{
	if (head == NULL)
	{
		addToHead(item);
	}
	else
	{
		Node<T> *curr = head;
		int c = length();
		int i = 1;
		Task x, y;
		x = item;
		y = curr->getData();
		bool f = 0;
		while (i <= c && !f)
		{
			if (x.getPriority() > y.getPriority())
			{
				addToIndex(item, i - 1);
				f = 1;
			}
			if (curr->getNext() == NULL)
			{
				break;
			}

			curr = curr->getNext();
			x = item;
			y = curr->getData();
			i++;
		}
		if (!f)
		{
			addToTail(item);
		}
	}
}

template <typename T>
T TasksList<T>::removeHead()
{
	Node<T> *curr = getHead();
	T s;
	if(curr!=NULL)
    {
        s = curr->getData();
        setHead(curr->getNext());
        delete curr;
        if(head!=NULL)
            head->setPrev(NULL);
        else tail = NULL;
    }
	return s;
}

template <class T>
T TasksList<T>::removeTail()
{
	Node<T> *curr = getTail();
	Task s;
	if(curr!=NULL)
    {
        s = curr->getData();
        setTail(curr->getPrev());
        delete curr;
        if(tail!=NULL)
            tail->setNext(NULL);
        else head = tail;
    }
	return s;
}

template <typename T>
T TasksList<T>::removeFromIndex(int i)
{
	int size = length();
	if (i==1)
	{
		Task s;
		removeHead();
		return s;
	}
	else if (i == size)
	{
		Task s;
		removeTail();
		return s;
	}
	Node<T> *curr = getHead();
	Node<T> *prev = NULL;
	Task s;
	int j = 1;
	while (j < i)
	{
		prev = curr;
		curr = curr->getNext();
		j++;
	}
	s = curr->getData();
	prev->setNext( curr->getNext());
	delete curr;
	curr = prev->getNext();
	curr->setPrev(prev);
	return s;
}

template <typename T>
T TasksList<T>::removeItem(char item[])
{
	Node<T> *curr = head;
	Node<T> *prev = NULL;
	int size = length();
	int i = 1;
	Task s;
	while (i <= size)
	{
		s = curr->getData();
		if (strcmp(s.getName(),item)==0)
		{
		    if(i==1)
            {
                removeHead();
                break;
            }
            if(i==size)
            {
                removeTail();
                break;
            }
			prev->setNext(curr->getNext());
			delete curr;
			curr = prev->getNext();
			curr->setPrev(prev);
			return s;
			break;
		}
		i++;
		prev = curr;
		curr = curr->getNext();
	}
}

template < typename T >
void TasksList<T>::removeWithPredicate(bool(*predicate)(T item1, char data[]), char item[])
{
    int size = length();
    Node<T> *curr = head;
    int x = 0;
    for (int i = 1; i <= size; i++)
    {
        Task record = curr->getData();
        if ((*predicate)(record,item))
        {
            removeFromIndex(i - x);
            x++;
        }
        curr = curr->getNext();
    }
}

template < typename T >
void TasksList<T>::sort(bool (*sortCriteria)(T item1, T item2), bool order) //using insertion sort
{
    int listLen=length();
    if(listLen<2)
        return;
    Node<T> *temp=head->getNext();
    for(int i=1;i<listLen;i++)
    {
        Node<T> *traverser=temp->getPrev();
        int j;
        for(j=i-1;j>=0 && traverser!=NULL && !(!sortCriteria(temp->getData(),traverser->getData())^order);j--)
            traverser=traverser->getPrev();
        if(i>j+1)//i.e. if the task is not in the proper place
        {
            addToIndex(temp->getData(),j+1);
            removeFromIndex(i+2);
        }
        temp=temp->getNext();
    }
}

template <class T>
void TasksList<T>::merge(TasksList<T> anotherList)
{
    short choice,order;
    Node<T> *ptr=anotherList.head;
    while(ptr!=NULL)
    {
        addToTail(ptr->getData());
        ptr=ptr->getNext();
    }
    cout<<"Choose how would you like to sort the merged list:\n\t1-By priority\n\t2-By due date\n\t3-By Duration\nYour choice: ";
    cin>>choice;
    cout<<"Choose sorting order:\n\t1-Ascending\n\t2-Descending\nYour choice: ";
    cin>>order;
    switch(choice)
    {
        case 2: sort(FuncLibrary::orderByDueDate,order-1);
                break;
        case 3: sort(FuncLibrary::orderByDuration,order-1);
                break;
        default:sort(FuncLibrary::orderByPriority,order-1);
    }
    cout<<"Done.. press any key to go back to menu...";
    cin.ignore();
    cin.get();
}

template <class T>
bool TasksList<T>::search(T item) {
	Node<T>* ptr = head;
	while (!(ptr == NULL) && item != ptr->getData())
		ptr = ptr->getNext();
	if (ptr != NULL)
		return true;
	else
		return false;
}

template <class T>
void TasksList<T>::reverse()
{
    vector<Task> vec;
    int len=length(),i;
    for(i=0;i<len;i++)
    {
        vec.push_back(getHead()->getData());
        removeHead();
    }
    for(i=0;i<len;i++)
    {
        addToTail(vec.back());
        vec.pop_back();
    }
}

template < typename T >
void TasksList<T>::display(int index)
{
	Node<T> *curr = head;
	int x = length();
	if (x != 0)
	{
		if (index == 0)
		{
			int i = 1;
			while (curr != NULL)
			{
				Task record;
				cout << "Task \" " << i << " \"\n";
				record = curr->getData();
				cout << record << endl;
				curr = curr->getNext();
				i++;
			}
		}
		else
		{
			int i = 1;
			Task record;
			while (i<x)
			{
				curr = curr->getNext();
				i++;
			}
			cout << "Task \" " << i << " \"\n";
			record = curr->getData();
			cout << record << endl;
		}
	}
	else
	{
		cout << "No tasks found!!\n";
	}
}

template < typename T >
int TasksList<T>::length()
{
	Node<T> *curr = head;
	int count = 0;
	while (curr != NULL)
	{
		count++;
		curr = curr->getNext();
	}
	return count;
}

template < typename T >
bool TasksList<T>::is_empty(std::ifstream& pFile)
{
	return pFile.peek() == std::ifstream::traits_type::eof();
}

template <class T>
ostream& operator<<(ostream& stream, TasksList<T> list) {
	if (list.head != 0) {
		Node<T>* ptr = list.head;
		stream << *ptr;
		while (ptr != list.tail) {
			ptr = ptr->getNext();
			stream << *ptr;
		}
		stream << "NULL" << endl;
	}
	return stream;
}

template < typename T >
void TasksList<T>::clear()
{
    while(head!=NULL)
        removeHead();
}

template < typename T >
void TasksList<T>::save()
{
	fstream file;
	file.open("MyTasks.txt", ios::out | ios::trunc);
	file.seekp(0);
	int x = length();
	Node<T> * curr = head;
	file << x;
	file << '\n';
	for (int i = 0; i < x; i++)
	{
		Task record = curr->getData();
		file << record.getName();
		file << '\n';
		file << record.getDescription();
		file << '\n';
		file << record.getPriority();
		file << '\n';
		file << record.getDueDate();
		file << '\n';
		file << record.getNumberOfDays();
		file << '\n';
		curr = curr->getNext();
	}
	file.close();
}

template < typename T >
TasksList<T>::~TasksList()
{
	save();
}

#endif
