#ifndef BST_H
#define BST_H
#include <fstream>
#include <cstring>
#include "Tree.h"
#include "TasksList.h"

#include <conio.h>

template <typename T>
class BST: public Tree<T>
{
private:
    const bool saveWhenDestructed;
protected:
    void loadFile();
    void listify(TasksList<T>& tasksList,TreeNode<T>* BSTRoot,bool method=0);//loads BST into a tasksList
                                                                             //method=0 -> order by priority
                                                                             //method=1 -> order by name
public:
    BST(bool saveWhenDestruc= true);
    virtual void insert(T item); //inserts in order (ordered by name)
    virtual void remove(TreeNode<T>* target); //keeps it ordered by name
    TreeNode<T>* search(string name);
    void removeTopPriority();
    void listWithPriority();//Displays all tasks sorted by priority
    TreeNode<T>* traverse(TreeNode<T>* node);//returns node with maximum priority
    bool is_empty(std::ifstream& pFile);
    void display(TreeNode<T>* node);//displays ALL tasks
    void save();
    void clear(TreeNode<T>* node);//Ought to take the root and clear the whole BST
    ~BST();
};
#include "BST.cpp"
#endif // BST_H
