#ifndef NODE_H
#define NODE_H
#include <conio.h>
#include <stdlib.h>
#include "Task.h"

using namespace std;

template <typename T>
class Node {
private:
	T     data;        // to hold generic data of type T
	Node<T>* next;     // to hold pointer to next item
	Node<T>* prev;     // to hold pointer for previous item
public:
	Node();
	Node(T dataItem, Node<T>* nextPtr = NULL, Node<T>* prevPtr = NULL);

	Node<T>* getNext() const;
	Node<T>* getPrev() const;
	T getData() const;
	void setNext(Node<T>* ptr);
	void setPrev(Node<T>* ptr);
	void setData(T dataItem);
	friend ostream& operator<< (ostream& out, Node<T> node);
};

#include "Node.cpp"
#endif
