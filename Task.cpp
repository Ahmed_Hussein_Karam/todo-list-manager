#include "Task.h"

char* Task::getName() const
{
    return (char*) Name;
}

Task::Task(const Task &task)
{
    strcpy(Name,task.getName());
    strcpy(Description,task.getDescription());
    Priority=task.getPriority();
    strcpy(Due_Date,task.getDueDate());
    Number_Of_Days=task.getNumberOfDays();
}

Task& Task::operator=(const Task &task)
{
    strcpy(Name,task.getName());
    strcpy(Description,task.getDescription());
    Priority=task.getPriority();
    strcpy(Due_Date,task.getDueDate());
    Number_Of_Days=task.getNumberOfDays();
    return *this;
}

char* Task::getDescription() const
{
    return (char*) Description;
}

double Task::getPriority() const
{
	    return Priority;
}

char* Task::getDueDate() const
{
	    return (char*) Due_Date;
}

int Task::getNumberOfDays() const
{
	    return Number_Of_Days;
}

void Task::setPriority(double priority)
{
    Priority = priority;
}

void Task::setNumberOfDays(int numberOfDays)
{
    Number_Of_Days=numberOfDays;
}

bool Task::validScript(char script[100]) const
{
    for(unsigned int i=0;i<strlen(script);i++)
        if(!isspace(script[i]))
            return strlen(script)<100;
    return 0;
}

bool Task::validDate(char date[11]) const
{
    if(strlen(date)!=10)
        return 0;
    int maxNumOfMonthDays[]={0,31,29,31,30,31,30,31,31,30,31,30,31};
    int day,month,year;
    day=10*(date[0]-'0')+date[1]-'0';
    if(!(isdigit(date[0]) && isdigit(date[1]) && date[2]=='/' && day>=1 && day<=31))
        return 0;
    month=10*(date[3]-'0')+date[4]-'0';
    if(!(isdigit(date[3]) && isdigit(date[4]) && date[5]=='/' && month>=1 && month<=12))
        return 0;
    year=1000*(date[6]-'0')+100*(date[7]-'0')+10*(date[8]-'0')+date[9]-'0';
    if(!(isdigit(date[6]) && isdigit(date[7]) && isdigit(date[8]) && isdigit(date[9]) && year>=2016))
        return 0;
    if(day>maxNumOfMonthDays[month])
        return 0;
    if(month==2 && (((year%4!=0 || year%400 != year%100) && day>28) || day>29))
       return 0;
    return 1;
}

istream& operator>>(istream & in, Task &record)
{
		cout << "Enter New Note Book Record\n\n";
		cout << "Task name: ";
		in.getline(record.Name, 100, '\n');
		while(!record.validScript(record.Name))
        {
            if(strlen(record.Name)>=100)
                cout<<"Too long task name!!\n\nPlease re-enter task name: ";
            in.getline(record.Name, 100, '\n');
        }
		cout << "Description: ";
		in.getline(record.Description, 100, '\n');
		while(!record.validScript(record.Description))
        {
            if(strlen(record.Description)>=100)
                cout<<"Too long task description!!\n\nPlease re-enter task description: ";
            in.getline(record.Description, 100, '\n');
        }
		cout << "Priority: ";
		in >> record.Priority;
		while(record.Priority>5||record.Priority<1)
        {
            cout<<"Priority Should be between 1-5\n";
            cout << "Priority: ";
            in >> record.Priority;
        }
		cout << "Due date (dd/mm/yyyy): ";
		cin.ignore();
		in.getline(record.Due_Date, 11, '\n');
		while(!record.validDate(record.Due_Date))
        {
            cout<<"Invalid date: either the date has already passed or the date format is wrong.\n\n"
                <<"Please re-enter date (dd/mm/yyyy): ";
            in.getline(record.Due_Date, 11, '\n');
        }
		cout << "Number of days: ";
		in>>record.Number_Of_Days;
		return in;
}

ostream& operator<<(ostream & out, Task &record)
{
		out << "Task name: " << record.Name << endl;
		out << "Description: " << record.Description << endl;
		out << "Priority: " << record.Priority << endl;
		out << "Due date: " << record.Due_Date << endl;
		out << "Number of days: " << record.Number_Of_Days << endl;
		return out;
}
