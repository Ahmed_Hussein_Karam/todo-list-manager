#ifndef AVL_H
#define AVL_H
#include <cmath>
#include <stack>
#include "BST.h"

template <typename T>
class AVL:public BST<T>
{
private:
    const bool saveWhenDestructed;
public:
    AVL(bool saveWhenDestruc= true);
    virtual void insert(T item); //inserts in order (ordered by name) and keeps it balanced
    virtual void remove(TreeNode<T>* target); //keeps it ordered by name and balanced
    TreeNode<T>* balance(TreeNode<T> *node);
    int balanceFactor(TreeNode<T> *node);//leftBalance - rightBalance
    int height(TreeNode<T> *node);//returns -1 for a null, 0 for a leaf, height otherwise
    TreeNode<T>* rotateRight(TreeNode<T> *node);
    TreeNode<T>* rotateLeft(TreeNode<T> *node);
    ~AVL();
};
#include "AVL.cpp"
#endif // AVL_H
