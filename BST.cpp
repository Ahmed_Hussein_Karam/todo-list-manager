#ifndef BST_CPP
#define BST_CPP
#include "BST.h"

template <typename T>
BST<T>::BST(bool saveWhenDestruc):saveWhenDestructed(saveWhenDestruc)
{
    this->root=NULL;
    loadFile();
}

template <typename T>
void BST<T>::loadFile()
{
    double tempPriority; //Used to setPriority()
    int tempDuration; //Used to setNumberOfDays()
    ifstream file;
	file.open("MyTasks.txt", ios::in);
	Task R;
	if (!is_empty(file))
	{
		file.seekg(0);
		int x;
		file >> x;
		int i = 0;
		while (i<x/*!file.eof()*/)
		{
		    file.ignore();
			file.getline(R.getName(), 100, '\n');
			file.getline(R.getDescription(), 100, '\n');
			file >> tempPriority;
			R.setPriority(tempPriority);
			file.ignore();
			file.getline(R.getDueDate(), 11, '\n');
			file>>tempDuration;
			R.setNumberOfDays(tempDuration);
			insert(R);
			i++;
		}
	}
	file.close();
}

template <typename T>
void BST<T>::listify(TasksList<T>& tasksList,TreeNode<T>* BSTRoot,bool method)
{
    if(BSTRoot!=NULL)
    {
        listify(tasksList,BSTRoot->getLeft());
        if(method==0)
            tasksList.addInOrder(BSTRoot->getData());
        else
            tasksList.addToTail(BSTRoot->getData());
        listify(tasksList,BSTRoot->getRight());
    }
}

template <typename T>
TreeNode<T>* BST<T>::search(string name)
{
    int number_comp=0;
    TreeNode<T>* temp=this->root;
    while(temp!=NULL)
    {
        number_comp++;
        if(strcmp(name.c_str(),temp->getData().getName()) == 0)
        {
            cout<<"\n\tNumber of comparisons = "<<number_comp<<'\n';
            return temp;
        }
        else if(strcmp(name.c_str(),temp->getData().getName()) > 0)
            temp=temp->getRight();
        else
            temp=temp->getLeft();
    }
    cout<<"\n\tNumber of comparisons = "<<number_comp<<'\n';
    return temp;
}

template <typename T>
void BST<T>::insert(T item)
{
    int number_comp=0;
    TreeNode<T>* temp=this->root;
    TreeNode<T>* newNode=new TreeNode<T>{};
    newNode->setData(item);
    if(this->root==NULL)
    {
        number_comp++;
        this->root=newNode;
        cout<<"\n\tNumber of comparisons = "<<number_comp<<'\n';
        return;
    }
    while(true)
    {
        number_comp++;
        if(strcmp(newNode->getData().getName(),temp->getData().getName())> 0)
            if(temp->getRight()==NULL)
            {
                temp->setRight(newNode);
                cout<<"\n\tNumber of comparisons = "<<number_comp<<'\n';
                return;
            }
            else temp=temp->getRight();
        else
            if(temp->getLeft()==NULL)
            {
                temp->setLeft(newNode);
                cout<<"\n\tNumber of comparisons = "<<number_comp<<'\n';
                return;
            }
            else temp=temp->getLeft();
    }
}

template <typename T>
void BST<T>::remove(TreeNode<T>* target)
{
    static int number_comp=0;
    if(target==NULL)
    {
        cout<<"\n\tNumber of comparisons = "<<number_comp<<'\n';
        return;
    }
    TreeNode<T>* temp=this->root;
    TreeNode<T>* parent=NULL;
    while(temp!=target && temp!=NULL)
    {
        number_comp++;
        parent=temp;
        if(strcmp(target->getData().getName(),temp->getData().getName()) > 0)
            temp=temp->getRight();
        else temp=temp->getLeft();
    }
    if(temp==NULL)
    {
        cout<<"\n\tNumber of comparisons = "<<number_comp<<'\n';
    }
    else if(temp->getLeft()==NULL && temp->getRight()==NULL)//No children
    {
        if(parent!=NULL)
        {
            if(temp==parent->getLeft())
                parent->setLeft(NULL);
            else
                parent->setRight(NULL);

        }
        else
            this->root=NULL;
        delete temp;
        cout<<"\n\tNumber of comparisons = "<<number_comp<<'\n';
    }
    else if(temp->getLeft()==NULL || temp->getRight()==NULL)//only one child
    {
        if(parent!=NULL)
        {
            if(temp==parent->getLeft())
            {
                if(temp->getLeft()!=NULL)
                    parent->setLeft(temp->getLeft());
                else
                    parent->setLeft(temp->getRight());
            }
            else
            {
                if(temp->getLeft()!=NULL)
                    parent->setRight(temp->getLeft());
                else
                    parent->setRight(temp->getRight());
            }
            delete temp;
        }
        else
        {
            if(temp->getLeft()!=NULL)
                this->root=temp->getLeft();
            else
                this->root=temp->getRight();
            delete temp;
        }
        cout<<"\n\tNumber of comparisons = "<<number_comp<<'\n';
    }
    else//two children
    {
        TreeNode<T>* maxLeft=temp->getLeft();
        while(maxLeft->getRight()!=NULL)
        {
            maxLeft=maxLeft->getRight();
            number_comp++;
        }
        temp->setData(maxLeft->getData());
        remove(maxLeft);
    }
}

template <typename T>
void BST<T>::removeTopPriority()
{
    remove(traverse(this->root));
}

template <typename T>
void BST<T>::listWithPriority()
{
    TasksList<T> tasksList;//Since BST is sorted by name, this list is used to sort them by priority without affecting BST
    tasksList.clear();
    listify(tasksList,this->root);
    tasksList.reverse();//To be in descending order
    tasksList.display();
    tasksList.clear();//Not to be saved to the file by the destructor
}

template <typename T>
TreeNode<T>* BST<T>::traverse(TreeNode<T>* visited)
{
    if(visited!=NULL)
    {
        TreeNode<T>* temp1;
        TreeNode<T>* temp2;
        temp2=visited;
        temp1=traverse(visited->getLeft());
        if(temp1!=NULL && temp1->getData().getPriority() > temp2->getData().getPriority())
            temp2=temp1;
        temp1=traverse(visited->getRight());
        if(temp1!=NULL && temp1->getData().getPriority() > temp2->getData().getPriority())
            temp2=temp1;
        return temp2;
    }
    return visited;
}
template <typename T>
bool BST<T>::is_empty(std::ifstream& pFile)
{
    return pFile.peek() == std::ifstream::traits_type::eof();
}

template <typename T>
void BST<T>::display(TreeNode<T> *node)
{
    if(node!=NULL)
    {
        display(node->getLeft());
        Task task;
        task=node->getData();
        cout<<task;
        display(node->getRight());
    }
}

template <typename T>
void BST<T>::save()
{
    TasksList<T> tasksList;
    tasksList.clear();
    listify(tasksList,this->root,1);
    //tasksList is saved automatically by its destructor
}

template <typename T>
void BST<T>::clear(TreeNode<T>* node)
{
    if(node!=NULL)
    {
        clear(node->getLeft());
        delete node;
        clear(node->getRight());
    }
}

template <typename T>
BST<T>::~BST()
{
    if(saveWhenDestructed==true)
        save();
    clear(this->root);
}
#endif // BST_CPP
