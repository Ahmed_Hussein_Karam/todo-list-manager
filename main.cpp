#include <iostream>
#include <fstream>
#include <conio.h>
#include <windows.h>
#include <cstdlib>
#include <string>
#include "TasksList.h"
#include "TasksHeap.h"
#include "AVL.h"
#include "TasksTreap.h"

using namespace std;

HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE); // used for goto
COORD CursorPosition; // used for goto

void gotoXY(int, int); // function defined below if this is new to you.
void clc();
void Main_Menu();
void Heap_Menu(TasksHeap<Task>&task);
void BST_Menu(BST<Task>&task);
void AVL_Menu(AVL<Task>&task);
void Treap_Menu(TasksTreap<Task>&task);
void TasksList_Menu(TasksList<Task>&task);
void AddTask(TasksList<Task> &task);
void RemoveTask(TasksList<Task> &task);
void SortTasks(TasksList<Task> &task);
void DisplayTasks(TasksList<Task> &task);
void RemoveWithPredicate(TasksList<Task> &task);

int main()
{
    SetConsoleTitle("System management");
    Main_Menu();
	return 0;
}

void Main_Menu()
{
    clc();
	int menu_item = 0, x = 7;
	bool running = true;

	gotoXY(2, 5);
	cout << "Main Menu";
	gotoXY(2, 7);
	cout << "->";

	while (running)
	{
		gotoXY(4, 7);  cout << "1) Tasks List";
		gotoXY(4, 8);  cout << "2) Heap ";
		gotoXY(4, 9);  cout << "3) BST";
		gotoXY(4, 10);  cout<< "4) AVL";
		gotoXY(4, 11);  cout<< "5) Treap";
		gotoXY(4, 12); cout << "   Quit Program";


		system("pause>nul"); // the >nul bit causes it the print no message

		if (GetAsyncKeyState(VK_DOWN) && x != 12) //down button pressed
		{
			gotoXY(2, x);
			cout << "  ";
			x++;
			gotoXY(2, x);
			cout << "->";
			menu_item++;
			continue;

		}

		if (GetAsyncKeyState(VK_UP) && x != 7) //up button pressed
		{
			gotoXY(2, x);
			cout << "  ";
			x--;
			gotoXY(2, x);
			cout << "->";
			menu_item--;
			continue;
		}

		if (GetAsyncKeyState(VK_RETURN))  // Enter key pressed
		{
			system("cls");
			running = 0;
		}

	}
	switch (menu_item)
	{

	case 0:
	{
		clc();
		TasksList<Task>task;
		TasksList_Menu(task);
		break;
	}
	case 1:
	{
		clc();
		TasksHeap<Task>task;
        Heap_Menu(task);
		break;
	}
	case 2:
	{
		clc();
		BST<Task>task;
        BST_Menu(task);
		break;
	}
	case 3:
	{
		clc();
		AVL<Task>task;
        AVL_Menu(task);
		break;
	}
	case 4:
	{
		clc();
		TasksTreap<Task>task;
		Treap_Menu(task);
		break;
	}
	case 5:
	{
		clc();
		cout << "\n\nThe program has now terminated!!\n\n";
		running = false;
	}

	}

}

void Heap_Menu(TasksHeap<Task>&task)
{
    clc();
	int menu_item = 0, x = 7;
	bool running = true;

	gotoXY(2, 5);
	cout << "Heap Menu";
	gotoXY(2, 7);
	cout << "->";

	while (running)
	{
		gotoXY(4, 7);  cout << "1) Insert Task";
		gotoXY(4, 8);  cout << "2) Remove Top Priority";
		gotoXY(4, 9);  cout << "3) Search";
		gotoXY(4, 10);  cout<< "4) Display Tasks";
		gotoXY(4, 11);  cout<< "5) Save";
		gotoXY(4, 12); cout << "6) Main Menu";


		system("pause>nul"); // the >nul bit causes it the print no message

		if (GetAsyncKeyState(VK_DOWN) && x != 12) //down button pressed
		{
			gotoXY(2, x);
			cout << "  ";
			x++;
			gotoXY(2, x);
			cout << "->";
			menu_item++;
			continue;

		}

		if (GetAsyncKeyState(VK_UP) && x != 7) //up button pressed
		{
			gotoXY(2, x);
			cout << "  ";
			x--;
			gotoXY(2, x);
			cout << "->";
			menu_item--;
			continue;
		}

		if (GetAsyncKeyState(VK_RETURN))  // Enter key pressed
		{
			system("cls");
			running = 0;
		}

	}
	switch (menu_item)
	{

	case 0:
	{
		clc();
		cout<<"Add Task :\n";
		Task t;
		cin>>t;
		task.insert(t);
		cout<<"\nPress Enter";
		system("pause>nul");
		//task.save();
		Heap_Menu(task);
		break;
	}
	case 1:
	{
		clc();
        cout<<"Remove Root :\n";
		task.removeRoot();
		cout<<"\nPress Enter";
		system("pause>nul");
		//task.save();
		Heap_Menu(task);
		break;
	}
	case 2:
	{
		clc();
        cout<<"Search :\n";
		Task t;
		string name;
		cout<<"Enter Name Of Task :";
		cin>>name;
		t=task.search(name);
		if(t.getNumberOfDays()>=0)
            cout<<t;
		cout<<"\nPress Enter";
		system("pause>nul");
		//task.save();
		Heap_Menu(task);
		break;
	}
	case 3:
	{
		clc();
        cout<<"Display Tasks :\n";
		task.Display();
		cout<<"\nPress Enter";
		system("pause>nul");
		Heap_Menu(task);
		break;
	}
	case 4:
	{
		clc();
		cout<<"Save Tasks \n";
		task.save();
		cout<<"\nPress Enter";
		system("pause>nul");
		Heap_Menu(task);
		break;
	}
	case 5:
	{
		clc();
		task.save();
		Main_Menu();
		running = false;
	}

	}
}

void BST_Menu(BST<Task>&task)
{
    clc();
	int menu_item = 0, x = 7;
	bool running = true;

	gotoXY(2, 5);
	cout << "BST Menu";
	gotoXY(2, 7);
	cout << "->";

	while (running)
	{
		gotoXY(4, 7);  cout << "1) Insert Task";
		gotoXY(4, 8);  cout << "2) Remove Top Priority";
		gotoXY(4, 9);  cout << "3) Search";
		gotoXY(4, 10);  cout<< "4) Display Tasks";
		gotoXY(4, 11);  cout<< "5) Save";
		gotoXY(4, 12); cout << "6) Main Menu";


		system("pause>nul"); // the >nul bit causes it the print no message

		if (GetAsyncKeyState(VK_DOWN) && x != 12) //down button pressed
		{
			gotoXY(2, x);
			cout << "  ";
			x++;
			gotoXY(2, x);
			cout << "->";
			menu_item++;
			continue;

		}

		if (GetAsyncKeyState(VK_UP) && x != 7) //up button pressed
		{
			gotoXY(2, x);
			cout << "  ";
			x--;
			gotoXY(2, x);
			cout << "->";
			menu_item--;
			continue;
		}

		if (GetAsyncKeyState(VK_RETURN))  // Enter key pressed
		{
			system("cls");
			running = 0;
		}

	}
	switch (menu_item)
	{

	case 0:
	{
		clc();
		cout<<"Add Task :\n";
		Task t;
		cin>>t;
		task.insert(t);
		cout<<"\nPress Enter";
		system("pause>nul");
		//task.save();
		BST_Menu(task);
		break;
	}
	case 1:
	{
		clc();
        cout<<"Remove Root :\n";
		task.removeTopPriority();
		cout<<"\nPress Enter";
		system("pause>nul");
		//task.save();
		BST_Menu(task);
		break;
	}
	case 2:
	{
		clc();
        cout<<"Search :\n";
		Task t;
		TreeNode<Task>* tt;
		string name;
		cout<<"Enter Name Of Task :";
		cin>>name;
		tt=task.search(name);
		if(tt!=NULL)
        {
            t=tt->getData();
            cout<<t;
        }
        else cout<<"\nTask does not exist.";
		cout<<"\nPress Enter";
		system("pause>nul");
		//task.save();
		BST_Menu(task);
		break;
	}
	case 3:
	{
		clc();
        cout<<"Display Tasks :\n";
		task.display(task.getRoot());
		cout<<"\nPress Enter";
		system("pause>nul");
		BST_Menu(task);
		break;
	}
	case 4:
	{
		clc();
		cout<<"Save Tasks :\n";
		task.save();
		cout<<"\nPress Enter";
		system("pause>nul");
		BST_Menu(task);
		break;
	}
	case 5:
	{
		clc();
		task.save();
		Main_Menu();
		running = false;
	}

	}
}

void AVL_Menu(AVL<Task>&task)
{
    clc();
	int menu_item = 0, x = 7;
	bool running = true;

	gotoXY(2, 5);
	cout << "AVL Menu";
	gotoXY(2, 7);
	cout << "->";

	while (running)
	{
		gotoXY(4, 7);  cout << "1) Insert Task";
		gotoXY(4, 8);  cout << "2) Remove Top Priority";
		gotoXY(4, 9);  cout << "3) Search";
		gotoXY(4, 10);  cout<< "4) Display Tasks";
		gotoXY(4, 11);  cout<< "5) Save";
		gotoXY(4, 12); cout << "6) Main Menu";


		system("pause>nul"); // the >nul bit causes it the print no message

		if (GetAsyncKeyState(VK_DOWN) && x != 12) //down button pressed
		{
			gotoXY(2, x);
			cout << "  ";
			x++;
			gotoXY(2, x);
			cout << "->";
			menu_item++;
			continue;

		}

		if (GetAsyncKeyState(VK_UP) && x != 7) //up button pressed
		{
			gotoXY(2, x);
			cout << "  ";
			x--;
			gotoXY(2, x);
			cout << "->";
			menu_item--;
			continue;
		}

		if (GetAsyncKeyState(VK_RETURN))  // Enter key pressed
		{
			system("cls");
			running = 0;
		}

	}
	switch (menu_item)
	{

	case 0:
	{
		clc();
		cout<<"Add Task :\n";
		Task t;
		cin>>t;
		task.insert(t);
		cout<<"\nPress Enter";
		system("pause>nul");
		//task.save();
		AVL_Menu(task);
		break;
	}
	case 1:
	{
		clc();
        cout<<"Remove Root :\n";
		task.removeTopPriority();
		cout<<"\nPress Enter";
		system("pause>nul");
		//task.save();
		AVL_Menu(task);
		break;
	}
	case 2:
	{
		clc();
        cout<<"Search :\n";
		Task t;
		TreeNode<Task>* tt;
		string name;
		cout<<"Enter Name Of Task :";
		cin>>name;
		tt=task.search(name);
		if(tt!=NULL)
        {
            t=tt->getData();
            cout<<t;
        }
        else cout<<"\nTask does not exist.";
		cout<<"\nPress Enter";
		system("pause>nul");
		//task.save();
		AVL_Menu(task);
		break;
	}
	case 3:
	{
		clc();
        cout<<"Display Tasks :\n";
		task.display(task.getRoot());
		cout<<"\nPress Enter";
		system("pause>nul");
		AVL_Menu(task);
		break;
	}
	case 4:
	{
		clc();
		cout<<"Save Tasks :\n";
		task.save();
		cout<<"\nPress Enter";
		system("pause>nul");
		AVL_Menu(task);
		break;
	}
	case 5:
	{
		clc();
		task.save();
		Main_Menu();
		running = false;
	}

	}

}


void Treap_Menu(TasksTreap<Task>&task)
{
    clc();
	int menu_item = 0, x = 7;
	bool running = true;

	gotoXY(2, 5);
	cout << "Treap Menu";
	gotoXY(2, 7);
	cout << "->";

	while (running)
	{
		gotoXY(4, 7);  cout << "1) Insert Task";
		gotoXY(4, 8);  cout << "2) Remove Root";
		gotoXY(4, 9);  cout << "3) Search";
		gotoXY(4, 10);  cout<< "4) Display Tasks";
		gotoXY(4, 11);  cout<< "5) Save";
		gotoXY(4, 12); cout << "6) Main Menu";

		system("pause>nul"); // the >nul bit causes it the print no message

		if (GetAsyncKeyState(VK_DOWN) && x != 12) //down button pressed
		{
			gotoXY(2, x);
			cout << "  ";
			x++;
			gotoXY(2, x);
			cout << "->";
			menu_item++;
			continue;

		}

		if (GetAsyncKeyState(VK_UP) && x != 7) //up button pressed
		{
			gotoXY(2, x);
			cout << "  ";
			x--;
			gotoXY(2, x);
			cout << "->";
			menu_item--;
			continue;
		}

		if (GetAsyncKeyState(VK_RETURN))  // Enter key pressed
		{
			system("cls");
			running = 0;
		}

	}
	switch (menu_item)
	{

	case 0:
	{
		clc();
		cout<<"Add Task :\n";
		Task t;
		cin>>t;
		task.insert(t);
		cout<<"\nPress Enter";
		system("pause>nul");
		//task.save();
		Treap_Menu(task);
		break;
	}
	case 1:
	{
		clc();
        cout<<"Remove Root :\n";
		task.removeRoot();
		cout<<"\nPress Enter";
		system("pause>nul");
		Treap_Menu(task);
		break;
	}
	case 2:
	{
		clc();
        cout<<"Search :\n";
		Task t;
		TreeNode<Task>* tt;
		string name;
		cout<<"Enter Name Of Task :";
		cin>>name;
		tt=task.search(name);
		if(tt!=NULL)
        {
            t=tt->getData();
            cout<<t;
        }
        else cout<<"\nTask does not exist.";
		cout<<"\nPress Enter";
		system("pause>nul");
		//task.save();
		Treap_Menu(task);
		break;
	}
	case 3:
	{
		clc();
        cout<<"Display Tasks :\n";
		task.Display();
		cout<<"\nPress Enter";
		system("pause>nul");
		Treap_Menu(task);
		break;
	}
	case 4:
	{
		clc();
		cout<<"Save Tasks Done :";
		task.save();
		cout<<"\nPress Enter";
		system("pause>nul");
		Treap_Menu(task);
		break;
	}
	case 5:
	{
		clc();
		task.save();
		Main_Menu();
		running = false;
	}

	}

}


void TasksList_Menu(TasksList<Task>&task)
{
	clc();
	int menu_item = 0, x = 7;
	bool running = true;

	gotoXY(2, 5);
	cout << "Tasks List Menu";
	gotoXY(2, 7);
	cout << "->";

	while (running)
	{
		gotoXY(4, 7);  cout << "1) Add Task";
		gotoXY(4, 8);  cout << "2) Remove Task ";
		gotoXY(4, 9);  cout << "3) Sort Tasks";
		gotoXY(4, 10);  cout<< "4) Display Tasks";
		gotoXY(4, 11);  cout<< "5) Save Tasks";
		gotoXY(4, 12); cout << "6) Main Menu";


		system("pause>nul"); // the >nul bit causes it the print no message

		if (GetAsyncKeyState(VK_DOWN) && x != 12) //down button pressed
		{
			gotoXY(2, x);
			cout << "  ";
			x++;
			gotoXY(2, x);
			cout << "->";
			menu_item++;
			continue;

		}

		if (GetAsyncKeyState(VK_UP) && x != 7) //up button pressed
		{
			gotoXY(2, x);
			cout << "  ";
			x--;
			gotoXY(2, x);
			cout << "->";
			menu_item--;
			continue;
		}

		if (GetAsyncKeyState(VK_RETURN))  // Enter key pressed
		{
			system("cls");
			running = 0;
		}

	}
	switch (menu_item)
	{

	case 0:
	{
		clc();
		AddTask(task);
		break;
	}
	case 1:
	{
		clc();
        RemoveTask(task);
		break;
	}
	case 2:
	{
		clc();
        SortTasks(task);
		break;
	}
	case 3:
	{
		clc();
        DisplayTasks(task);
		break;
	}
	case 4:
	{
		clc();
		task.save();
        cout<<"\n\n\t\tDone Save\n\n";
		cout << " press Enter To GoTO Back ";
		system("pause>nul"); // the >nul bit causes it the print no message
		TasksList_Menu(task);
		break;
	}
	case 5:
	{
		clc();
		task.save();
		Main_Menu();
		break;
	}

	}

}

void RemoveTask(TasksList<Task> &task)
{
	clc();
	int menu_item = 0, x = 7;
	bool running = true;
	gotoXY(2, 5);
	cout << "Remove Menu";
	gotoXY(2, 7);
	cout << "->";

	while (running)
	{
		gotoXY(4, 7);  cout << "1) Remove first task";
		gotoXY(4, 8);  cout << "2) Remove last task";
		gotoXY(4, 9);  cout << "3) Remove from index";
		gotoXY(4, 10);  cout <<"4) Remove task \" with task name \"";
		gotoXY(4, 11);  cout <<"5) Remove with predicate";
		gotoXY(4, 12);  cout <<"6) Remove all tasks";
		gotoXY(4, 13); cout << "7) Tasks List Menu";


		system("pause>nul"); // the >nul bit causes it the print no message

		if (GetAsyncKeyState(VK_DOWN) && x != 13) //down button pressed
		{
			gotoXY(2, x);
			cout << "  ";
			x++;
			gotoXY(2, x);
			cout << "->";
			menu_item++;
			continue;

		}

		if (GetAsyncKeyState(VK_UP) && x != 7) //up button pressed
		{
			gotoXY(2, x);
			cout << "  ";
			x--;
			gotoXY(2, x);
			cout << "->";
			menu_item--;
			continue;
		}

		if (GetAsyncKeyState(VK_RETURN))  // Enter key pressed
		{
			system("cls");
			running = 0;
		}

	}
	switch (menu_item)
	{

	case 0:
	{
		clc();
		cout<<" Remove first task:\n";
		task.removeHead();
		cout<<"\n\n\t\tRemove Done\n\n";
		cout << " press Enter To GoTO Back ";
		system("pause>nul"); // the >nul bit causes it the print no message
		RemoveTask(task);
		break;
	}
	case 1:
	{
		clc();
		cout<<" Remove last task:\n";
		task.removeTail();
		cout<<"\n\n\t\tRemove Done\n\n";
		cout << " press Enter To GoTO Back ";
		system("pause>nul"); // the >nul bit causes it the print no message
		RemoveTask(task);
		break;
	}
	case 2:
	{
		clc();
		cout<<" Remove from index :\n";
		cout<<"\n\nEnter Index : ";
		int i;
		cin>>i;
		cin.ignore();
		clc();
		task.removeFromIndex(i);
		cout<<"\n\n\t\tRemove Done\n\n";
		cout << " press Enter To GoTO Back ";
		system("pause>nul"); // the >nul bit causes it the print no message
		RemoveTask(task);
		break;
	}
	case 3:
	{
		clc();
		cout<<" Remove task \" with task name \":\n";
		char t[100];
		cout<<"Enter task name: ";
		cin>>t;
		task.removeItem(t);
		clc();
		cout<<"\n\n\t\tRemove Done\n\n";
		cout << " press Enter To GoTO Back ";
		system("pause>nul"); // the >nul bit causes it the print no message
		RemoveTask(task);
		break;
	}
	case 4:
	{
		clc();
		RemoveWithPredicate(task);
		break;
	}
	case 5:
	{
		clc();
		task.clear();
		clc();
		cout<<"\n\n\t\tRemove Done\n\n";
		cout << " press Enter To GoTO Back ";
		system("pause>nul"); // the >nul bit causes it the print no message
		RemoveTask(task);
		break;
	}
	case 6:
	{
		clc();
		task.save();
		TasksList_Menu(task);
		running = false;
	}

	}
}

void RemoveWithPredicate(TasksList<Task> &task)
{
	clc();
	int menu_item = 0, x = 7;
	bool running = true;
	gotoXY(2, 5);
	cout << "Remove With Predicate Menu";
	gotoXY(2, 7);
	cout << "->";

	while (running)
	{
		gotoXY(4, 7);  cout << "1) Remove with name";
		gotoXY(4, 8);  cout << "2) Remove with priority";
		gotoXY(4, 9);  cout << "3) Remove expired tasks";
		gotoXY(4, 10);  cout <<"4) Remove with number of days";
		gotoXY(4, 11); cout << "5) Remove Menu";


		system("pause>nul"); // the >nul bit causes it the print no message

		if (GetAsyncKeyState(VK_DOWN) && x != 11) //down button pressed
		{
			gotoXY(2, x);
			cout << "  ";
			x++;
			gotoXY(2, x);
			cout << "->";
			menu_item++;
			continue;

		}

		if (GetAsyncKeyState(VK_UP) && x != 7) //up button pressed
		{
			gotoXY(2, x);
			cout << "  ";
			x--;
			gotoXY(2, x);
			cout << "->";
			menu_item--;
			continue;
		}

		if (GetAsyncKeyState(VK_RETURN))  // Enter key pressed
		{
			system("cls");
			running = 0;
		}

	}
	switch (menu_item)
	{

	case 0:
	{
		clc();
		cout<<" Remove with name: \n";
		char t[100];
		cout<<"Enter name of tasks: ";
		cin>>t;
		task.removeWithPredicate(FuncLibrary::isSameName,t);
		clc();
		cout<<"\n\n\t\tRemove Done\n\n";
		cout << " press Enter To GoTO Back ";
		system("pause>nul"); // the >nul bit causes it the print no message
		RemoveWithPredicate(task);
		break;
	}
	case 1:
	{
		clc();
		cout<<" Remove with priority:\n";
		char t[100];
		cout<<"Enter minimum priority of tasks that will remain: ";
		cin>>t;
		task.removeWithPredicate(FuncLibrary::isDwonPriority,t);
		clc();
		cout<<"\n\n\t\tRemove Done\n\n";
		cout << " press Enter To GoTO Back ";
		system("pause>nul"); // the >nul bit causes it the print no message
		RemoveWithPredicate(task);
		break;
	}
	case 2:
	{
		clc();
		cout<<" Remove expired tasks: \n";
		char t[11];
		cout<<"Enter today's date ( dd/mm/yyyy ): ";
		cin>>t;
		task.removeWithPredicate(FuncLibrary::isPassedDate,t);
		clc();
		cout<<"\n\n\t\tRemove Done\n\n";
		cout << " press Enter To GoTO Back ";
		system("pause>nul"); // the >nul bit causes it the print no message
		RemoveWithPredicate(task);
		break;
    }
	case 3:
	{
	    clc();
		cout<<" Remove with number of days: \n";
		char t[100];
		cout<<"Enter minimum number of days of tasks that will remain: ";
		cin>>t;
		task.removeWithPredicate(FuncLibrary::isDwonNoD,t);
		clc();
		cout<<"\n\n\t\tRemove Done\n\n";
		cout << " press Enter To GoTO Back ";
		system("pause>nul"); // the >nul bit causes it the print no message
		RemoveWithPredicate(task);
		break;
    }
	case 4:
	{
		clc();
		RemoveTask(task);
		running = false;
	}

	}
}

void SortTasks(TasksList<Task> &task)
{
    clc();
	int menu_item = 0, x = 7;
	bool running = true;
	gotoXY(2, 5);
	cout << "Sort Menu";
	gotoXY(2, 7);
	cout << "->";

	while (running)
	{
		gotoXY(4, 7);  cout << "1) Order By Duration \" Ascending \"";
		gotoXY(4, 8);  cout << "2) Order By Duration \" Descending \"";
		gotoXY(4, 9);  cout << "3) Order By Due Date \" Ascending \"";
		gotoXY(4, 10);  cout <<"4) Order By Due Date \" Descending \"";
		gotoXY(4, 11);  cout <<"5) Order By Priority \" Ascending \"";
		gotoXY(4, 12);  cout <<"6) Order By Priority \" Descending \"";
		gotoXY(4, 13); cout << "7) Tasks List Menu";

		system("pause>nul"); // the >nul bit causes it the print no message

		if (GetAsyncKeyState(VK_DOWN) && x != 13) //down button pressed
		{
			gotoXY(2, x);
			cout << "  ";
			x++;
			gotoXY(2, x);
			cout << "->";
			menu_item++;
			continue;

		}

		if (GetAsyncKeyState(VK_UP) && x != 7) //up button pressed
		{
			gotoXY(2, x);
			cout << "  ";
			x--;
			gotoXY(2, x);
			cout << "->";
			menu_item--;
			continue;
		}

		if (GetAsyncKeyState(VK_RETURN))  // Enter key pressed
		{
			system("cls");
			running = 0;
		}

	}
	switch (menu_item)
	{

	case 0:
	{
		clc();
		cout<<" Order By Duration : \n";
        task.sort(FuncLibrary::orderByDuration,0); //0 ascending, 1 descending
		cout<<"\n\n\t\tSort Done \n\n";
		cout << " press Enter To GoTO Back ";
		system("pause>nul"); // the >nul bit causes it the print no message
		SortTasks(task);
		break;
	}
	case 1:
	{
		clc();
		cout<<" Order By Duration : \n";
        task.sort(FuncLibrary::orderByDuration,1); //0 ascending, 1 descending
		cout<<"\n\n\t\tSort Done \n\n";
		cout << " press Enter To GoTO Back ";
		system("pause>nul"); // the >nul bit causes it the print no message
		SortTasks(task);
		break;
	}
	case 2:
	{
		clc();
		cout<<" Order By Due Date : \n";
        task.sort(FuncLibrary::orderByDueDate,0); //0 ascending, 1 descending
		cout<<"\n\n\t\tSort Done \n\n";
		cout << " press Enter To GoTO Back ";
		system("pause>nul"); // the >nul bit causes it the print no message
		SortTasks(task);
		break;
	}
	case 3:
	{
		clc();
		cout<<" Order By Due Date : \n";
        task.sort(FuncLibrary::orderByDueDate,1); //0 ascending, 1 descending
		cout<<"\n\n\t\tSort Done \n\n";
		cout << " press Enter To GoTO Back ";
		system("pause>nul"); // the >nul bit causes it the print no message
		SortTasks(task);
		break;
	}
	case 4:
	{
		clc();
		cout<<" Order By Priority : \n";
        task.sort(FuncLibrary::orderByPriority,0); //0 ascending, 1 descending
		cout<<"\n\n\t\tSort Done \n\n";
		cout << " press Enter To GoTO Back ";
		system("pause>nul"); // the >nul bit causes it the print no message
		SortTasks(task);
		break;
	}
	case 5:
	{
		clc();
		cout<<" Order By Priority : \n";
        task.sort(FuncLibrary::orderByPriority,1); //0 ascending, 1 descending
		cout<<"\n\n\t\tSort Done \n\n";
		cout << " press Enter To GoTO Back ";
		system("pause>nul"); // the >nul bit causes it the print no message
		SortTasks(task);
		break;
	}
	case 6:
	{
		clc();
		task.save();
		TasksList_Menu(task);
		running = false;
	}

	}
}


void AddTask(TasksList<Task> &task)
{
	clc();
	int menu_item = 0, x = 7;
	bool running = true;

	gotoXY(2, 5);
	cout << "Add Menu";
	gotoXY(2, 7);
	cout << "->";

	while (running)
	{
		gotoXY(4, 7);  cout << "1) Add To Head";
		gotoXY(4, 8);  cout << "2) Add To Tail ";
		gotoXY(4, 9);  cout << "3) Add To Index";
		gotoXY(4, 10);  cout <<"4) Add In Order \" Priority \" ";
		gotoXY(4, 11); cout << "5) Tasks List Menu";


		system("pause>nul"); // the >nul bit causes it the print no message

		if (GetAsyncKeyState(VK_DOWN) && x != 11) //down button pressed
		{
			gotoXY(2, x);
			cout << "  ";
			x++;
			gotoXY(2, x);
			cout << "->";
			menu_item++;
			continue;

		}

		if (GetAsyncKeyState(VK_UP) && x != 7) //up button pressed
		{
			gotoXY(2, x);
			cout << "  ";
			x--;
			gotoXY(2, x);
			cout << "->";
			menu_item--;
			continue;
		}

		if (GetAsyncKeyState(VK_RETURN))  // Enter key pressed
		{
			system("cls");
			running = 0;
		}

	}
	switch (menu_item)
	{

	case 0:
	{
		clc();
		cout<<" Add To Head : \n";
		Task t;
		cin>>t;
		task.addToHead(t);
		clc();
		cout<<"\n\n\t\tAdd Done\n\n";
		cout << " press Enter To GoTO Back ";
		system("pause>nul"); // the >nul bit causes it the print no message
		AddTask(task);
		break;
	}
	case 1:
	{
		clc();
		cout<<" Add To Tail : \n";
		Task t;
		cin>>t;
		task.addToTail(t);
		clc();
		cout<<"\n\n\t\tAdd Done\n\n";
		cout << " press Enter To GoTO Back ";
		system("pause>nul"); // the >nul bit causes it the print no message
		AddTask(task);
		break;
	}
	case 2:
	{
		clc();
		cout<<" Add To Index : \n";
		Task t;
		cin>>t;
		cout<<"\n\nEnter Index :";
		int i;
		cin>>i;
		cin.ignore();
		task.addToIndex(t,i);
		clc();
		cout<<"\n\n\t\tAdd Done\n\n";
		cout << " press Enter To GoTO Back ";
		system("pause>nul"); // the >nul bit causes it the print no message
		AddTask(task);
		break;
	}
	case 3:
	{
		clc();
		cout<<" Add In Order \" Priority \": \n";
		Task t;
		cin>>t;
		task.addInOrder(t);
		clc();
		cout<<"\n\n\t\tAdd Done\n\n";
		cout << " press Enter To GoTO Back ";
		system("pause>nul"); // the >nul bit causes it the print no message
		AddTask(task);
		break;
	}
	case 4:
	{
		clc();
		task.save();
		TasksList_Menu(task);
		running = false;
	}

	}
}

void DisplayTasks(TasksList<Task> &task)
{
    clc();
	int menu_item = 0, x = 7;
	bool running = true;

	gotoXY(2, 5);
	cout << "Display Menu";
	gotoXY(2, 7);
	cout << "->";

	while (running)
	{
		gotoXY(4, 7);  cout << "1) Display All Tasks";
		gotoXY(4, 8);  cout << "2) Display one Task ";
		gotoXY(4, 9); cout <<  "3) Main Menu";


		system("pause>nul"); // the >nul bit causes it the print no message

		if (GetAsyncKeyState(VK_DOWN) && x != 9) //down button pressed
		{
			gotoXY(2, x);
			cout << "  ";
			x++;
			gotoXY(2, x);
			cout << "->";
			menu_item++;
			continue;

		}

		if (GetAsyncKeyState(VK_UP) && x != 7) //up button pressed
		{
			gotoXY(2, x);
			cout << "  ";
			x--;
			gotoXY(2, x);
			cout << "->";
			menu_item--;
			continue;
		}

		if (GetAsyncKeyState(VK_RETURN))  // Enter key pressed
		{
			system("cls");
			running = 0;
		}

	}
	switch (menu_item)
	{

	case 0:
	{
		clc();
		cout<<"All Tasks:\n";
		task.display();
		cout<<"\n\n\t\tDone \n\n";
		cout << " press Enter To GoTO Back ";
		system("pause>nul"); // the >nul bit causes it the print no message
		DisplayTasks(task);
		break;
	}
	case 1:
	{
		clc();
		cout<<"\n\nEnter Index :";
		int i;
		cin>>i;
		cin.ignore();
		clc();
		task.display(i);
		cout<<"\n\n\t\tDone \n\n";
		cout << " press Enter To GoTO Back ";
		system("pause>nul"); // the >nul bit causes it the print no message
		DisplayTasks(task);
		break;
	}
	case 2:
	{
		clc();
		task.save();
		TasksList_Menu(task);
		running = false;
	}

	}
}

void gotoXY(int x, int y)
{
	CursorPosition.X = x;
	CursorPosition.Y = y;
	SetConsoleCursorPosition(console, CursorPosition);
}

void clc()
{
	system("cls");
	cout << "             *************************                 " << endl;
	cout << "***************  System Management  ****************" << endl;
	cout << "             *************************                 " << endl;
	gotoXY(0, 4);
}
