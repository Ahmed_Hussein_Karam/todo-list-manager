#ifndef TASKSTREAP_H
#define TASKSTREAP_H

#include <iostream>
#include <vector>
#include <string>
#include <cstring>
#include <fstream>
#include "TreeNode.h"
#include "Task.h"
#include "TasksHeap.h"
#include "AVL.h"

using namespace std;

template<typename T>
class TasksTreap: public AVL<T>
{
    public:
        TasksTreap();
        ~TasksTreap();
        virtual void insert(T item);
        TreeNode<T>* Insert(T  x, TreeNode<T> *  node ,int& number_comp);
        TreeNode<T>* search(string name);
        void removeRoot();
        void Remove(TreeNode<T>* node);
        void InOrder(TreeNode<T>* node);
        void Display();
    protected:
    private:
};


#include "TasksTreap.cpp"
#endif // TASKSTREAP_H
