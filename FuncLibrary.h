#ifndef FUNC_LIBRARY_H
#define FUNC_LIBRARY_H
#include <cstring>
#include <cstdlib>
#include "Task.h"

class FuncLibrary
{
public:
    friend void reverseDate(char original[11], char reversed[11]);
    static bool orderByPriority (Task task1, Task task2);
    static bool orderByDueDate (Task task1, Task task2);
    static bool orderByDuration (Task task1, Task task2);
    static bool isPassedDate(Task item,char data[]);
    static bool isDwonPriority(Task item,char data[]);//determines whether the task has less priority than that sent as parameter
    static bool isSameName(Task item,char data[]);
    static bool isDwonNoD(Task item,char data[]);//determines whether the task has less duration than that sent as parameter
};

#endif // FUNC_LIBRARY_H
