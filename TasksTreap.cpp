#ifndef TASKSTREAP_CPP
#define TASKSTREAP_CPP

#include "TasksTreap.h"

template<typename T>
TasksTreap<T>::TasksTreap()
{
    this->setRoot(NULL);
    this->loadFile();
}

template<typename T>
TasksTreap<T>::~TasksTreap()
{

}

template<typename T>
void TasksTreap<T>::insert(T item)
{
    static int number_comp=0;
    Insert( item, this->root ,number_comp);
    cout<<"\n\tNumber of comparisons = "<<number_comp<<'\n';

}


template<typename T>
TreeNode<T>* TasksTreap<T>:: Insert(T x, TreeNode<T> * node,int& number_comp)
{
    TreeNode<T>* p_node =  new TreeNode<T>(x,NULL,NULL);//New_Node(x);
    if(node==NULL)
        {
            number_comp++;
            this->setRoot( p_node);
            return (node=p_node);
        }
    if( strcmp(x.getName(), node->getData().getName())<=0)
    {
        number_comp++;
        node->setLeft( Insert(x, node->getLeft(),number_comp));
        if(node->getLeft()->getData().getPriority() > node->getData().getPriority())
            {
                number_comp++;
                node = this->rotateRight(node);
            }
    }
    else
    {
        number_comp++;
        node->setRight( Insert(x, node->getRight(),number_comp));
        if(node->getRight()->getData().getPriority() < node->getData().getPriority())
            {
                number_comp++;
                node = this->rotateLeft(node);
            }
    }
    return node;
}

template<typename T>
TreeNode<T>* TasksTreap<T>:: search(string name)
{
    int number_comp=0;
    TreeNode<T>* temp=this->getRoot();
    while(temp!=NULL)
    {
        number_comp++;
        if(strcmp(name.c_str(),temp->getData().getName()) == 0)
        {
            cout<<"\n\tNumber of comparisons = "<<number_comp<<'\n';
            return temp;
        }
        else if(strcmp(name.c_str(),temp->getData().getName()) > 0)
            temp=temp->getRight();
        else
            temp=temp->getLeft();
    }
    cout<<"\n\tNumber of comparisons = "<<number_comp<<'\n';
    return temp;
}

template <typename T>
void TasksTreap<T>:: removeRoot()
{
    this->removeTopPriority();
}

template<typename T>
void TasksTreap<T>::Remove(TreeNode<T>* node)
{
    if(node->getLeft()==NULL || node->getRight()==NULL )
    {
        TreeNode<Task>* temp = node;
        if(node->getLeft()==NULL )
            node = node->getRight();
        else
            node = node->getLeft();
        free(temp);
    }
    else
    {
        if(node->getLeft()->getData().getPriority() > node->getRight()->getData().getPriority())
        {
            node = this->rotateRight(node);
            Remove( node->getRight());
        }
        else
        {
            node = this->rotateLeft(node);
            Remove(node->getLeft());
        }
    }
}

template <typename T>
void TasksTreap<T>:: InOrder(TreeNode<T>* node)
{
	if(node==NULL)
	{return;}
		InOrder(node->getLeft());
		Task task;
        task=node->getData();
        cout<<task;
		InOrder(node->getRight());

}

template <typename T>
void TasksTreap<T>::Display()
{
    InOrder(this->root);
}

#endif
