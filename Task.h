#ifndef TASK_H
#define TASK_H
#include <iostream>
#include <cstring>
#include <strstream>

using namespace std;

class Task
{
private:
	char Name[100];
	char Description[100];
	double Priority;
	char Due_Date[11];
	int Number_Of_Days;
public:
	Task()=default;
	Task(const Task &task);
	Task &operator=(const Task &task);

	char* getName() const;
	char* getDescription() const;
	double getPriority() const;
    char* getDueDate() const;
	int getNumberOfDays() const;

	void setPriority(double priority);
	void setNumberOfDays(int numberOfDays);

    bool validScript(char script[100]) const; //to validate name, description
    bool validDate(char date[11]) const;

	friend istream& operator>>(istream & in, Task &record);
	friend ostream& operator<<(ostream & out, Task &record);

	~Task()=default;
};

#endif
