#ifndef TASKSHEAP_CPP
#define TASKSHEAP_CPP

#include "TasksHeap.h"

template <typename T>
TasksHeap<T>::TasksHeap()
{
    heapElements = new T[MaxSize];
	SizeOfArray = 0;
    LoadFile();
}

template <typename T>
void TasksHeap<T>::LoadFile()
{
    double tempPriority; //Used to setPriority()
    int tempDuration; //Used to setNumberOfDays()
	ifstream file;
	file.open("MyTasks.txt", ios::in);
	Task R;
	if (!is_empty(file))
	{
		file.seekg(0);
		int x;
		file >> x;
		int i = 0;
		while (i<x/*!file.eof()*/)
		{
		    file.ignore();
			file.getline(R.getName(), 100, '\n');
			file.getline(R.getDescription(), 100, '\n');
			file >> tempPriority;
			R.setPriority(tempPriority);
			file.ignore();
			file.getline(R.getDueDate(), 11, '\n');
			file>>tempDuration;
			R.setNumberOfDays(tempDuration);
			insert(R);
			i++;
		}
	}
	file.close();
}

template <typename T>
TasksHeap<T>::TasksHeap(T* heapElement, int siz)
{
	heapElements = new T[MaxSize];
	SizeOfArray = siz;

	// This copies the array into the heap's internal array
	for (int i = 0; i < siz; ++i)
		heapElements[i] = heapElement[i];

	// This organizes the Array into a proper HeapTree
	for (int i = ParentOf(SizeOfArray - 1); i >= 0; --i)
		ShiftDown(i);
}

template <typename T>
void TasksHeap<T>:: insert(T elem)
{
    int number_comp=0;
    	if (SizeOfArray >= MaxSize)    // If we have reached our maximum capacity
		{
		    cout<<" reached our maximum capacity of Array of Tasks \n\n";
		    cout<<"\n\tNumber of comparisons = "<<number_comp<<'\n';
		    return;
		}
	heapElements[SizeOfArray] = elem;
	SizeOfArray++;
	ShiftUp(SizeOfArray-1,number_comp);
	cout<<"\n\tNumber of comparisons = "<<number_comp<<'\n';
}

template <typename T>
T TasksHeap<T>:: removeRoot()
{
    int number_comp=0;
    if(SizeOfArray==0)
    {
        cout<<" Not Found Tasks \n";
        cout<<"\n\tNumber of comparisons = "<<number_comp<<'\n';
        T s;
        return s;
    }
	T Temp = heapElements[0];
	heapElements[0] = heapElements[--SizeOfArray];  // Replace with the last element
	ShiftDown(0,number_comp);
	cout<<"\n\tNumber of comparisons = "<<number_comp<<'\n';
	return Temp;
}

template <typename T>
T TasksHeap<T>:: search (string taskName)
{
    int number_comp=0;
    bool flag=0;
    for(int i=0;i<SizeOfArray;i++)
    {
        number_comp++;
        Task R=heapElements[i];
        if(strcmp(R.getName(),taskName.c_str())==0)
        {
            flag=1;
            cout<<"\n\tNumber of comparisons = "<<number_comp<<'\n';
            return heapElements[i];
        }
    }
    if(!flag)
    {
        cout<<"Not Found \n\n";
        Task s;
        s.setNumberOfDays(-1);
        cout<<"\n\tNumber of comparisons = "<<number_comp<<'\n';
        return s;
    }
}

template <typename T>
T* TasksHeap<T>:: heapSort()
{
    int siz=SizeOfArray;
	T * NewArray = new T[siz];
	for(int i=0; i <siz ; i++)
	{
		NewArray[i] = removeRoot();
	}
	for (int i=0; i <siz ; i++)
	{
		heapElements[i]= NewArray[i] ;
		SizeOfArray++;
	}
	return NewArray;
}

template <typename T>
void TasksHeap<T>:: ShiftUp(int Node,int &x)
{
    int  Current = Node;
    int Parent = ParentOf(Current);
	T Item = heapElements[Current];
    Task s=Item;
	while (Current > 0)
	{
	    x++;
	    Task ss=heapElements[Parent];
		if (ss.getPriority() < s.getPriority())
		{
			heapElements[Current] = heapElements[Parent];
			Current = Parent;
			Parent = ParentOf(Current);
		}
		else
			break;
	}
	heapElements[Current] = Item;
}

template <typename T>
void TasksHeap<T>:: ShiftDown(int Node,int&x)
{
    int Current = Node,Child = LeftChildOf(Current);
	T Item = heapElements[Current];
    Task s=Item;
	while (Child < SizeOfArray)
	{
	    x++;
	    Task ss=heapElements[Child];
		if (Child < (SizeOfArray - 1))
        {
            Task z=heapElements[Child+1];
			if (ss.getPriority() < z.getPriority())
				{
				    ++Child;
				    ss= z;
                }
        }
		if (s.getPriority() < ss.getPriority())
		{
			heapElements[Current] = heapElements[Child];
			Current = Child;
			Child = LeftChildOf(Current);
		}
		else
			break;
	}
	heapElements[Current] = Item;
}

template <typename T>
int TasksHeap<T>:: ParentOf(int Node)
{
    return ((Node-1)/2);
}

template <typename T>
int TasksHeap<T>::LeftChildOf(int Node)
{
	return ((Node * 2) + 1);
}

template <typename T>
int TasksHeap<T>::size()
{
    return SizeOfArray;
}

template <typename T>
void TasksHeap<T>::save()
{
	fstream file;
	file.open("MyTasks.txt", ios::out | ios::trunc);
	file.seekp(0);
	int x = SizeOfArray;
	file << x;
	file << '\n';
	for (int i = 0; i < x; i++)
	{
		Task record = heapElements[i];
		file << record.getName();
		file << '\n';
		file << record.getDescription();
		file << '\n';
		file << record.getPriority();
		file << '\n';
		file << record.getDueDate();
		file << '\n';
		file << record.getNumberOfDays();
		file << '\n';
	}
	file.close();
}
template <typename T>
TasksHeap<T>:: ~TasksHeap ()
{
    save();
    if (heapElements)
		delete heapElements;
}

template < typename T >
bool TasksHeap<T>::is_empty(std::ifstream& pFile)
{
	return pFile.peek() == std::ifstream::traits_type::eof();
}

template < typename T >
void TasksHeap<T>:: Display()
{
    heapSort();
    for(int i=0;i<SizeOfArray;i++)
    {
        Task t=heapElements[i];
        cout<<"Task "<<i+1<<" : \n";
        cout<<t;
    }
}

#endif
