#ifndef TREE_CPP
#define TREE_CPP
#include "Tree.h"

template <typename T>
TreeNode<T>* Tree<T>::getRoot() const
{
    return root;
}

template <typename T>
void Tree<T>::setRoot(TreeNode<T>* ptr)
{
    root=ptr;
}

template <typename T>
int Tree<T>::size(TreeNode<T> *node) const
{
    static int treeSize=0;
    if(node!=NULL)
    {
        treeSize++;
        size(node->getLeft());
        size(node->getRight());
    }
    return treeSize;
}
#endif // TREE_CPP
