#include "FuncLibrary.h"

void reverseDate(char original[11],char reversed[11])
{
    int i=0;
    for(;i<4;i++) reversed[i]=original[i+6];
    for(;i<8;i++) reversed[i]=original[i-2];
    for(;i<10;i++) reversed[i]=original[i-8];
}

bool FuncLibrary::orderByPriority (Task task1, Task task2)
{
    return (task1.getPriority() < task2.getPriority());
}
bool FuncLibrary::orderByDueDate (Task task1, Task task2)
{
    char date1[11],date2[11];
    reverseDate(task1.getDueDate(),date1);
    reverseDate(task2.getDueDate(),date2);
    return strcmp(date1, date2)<0;
}
bool FuncLibrary::orderByDuration (Task task1, Task task2)
{
    return task1.getNumberOfDays()<task2.getNumberOfDays();
}

bool FuncLibrary::isPassedDate(Task item,char data[])
{
    char date1[11],date2[11];
    reverseDate(item.getDueDate(),date1);
    reverseDate(data,date2);
    return strcmp(date1, date2)<0;
}

bool FuncLibrary::isDwonPriority(Task item,char data[])
{
    return item.getPriority()<int(data[0])-48;
}
bool FuncLibrary::isSameName(Task item,char data[])
{
    return strcmp(item.getName(),data)==0;
}
bool FuncLibrary::isDwonNoD(Task item,char data[])//
{
    return item.getNumberOfDays()<atoi(data);
}
