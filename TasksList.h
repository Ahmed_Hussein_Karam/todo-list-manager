#ifndef TASKS_LIST_H
#define TASKS_LIST_H
#include <iostream>
#include <vector>
#include <string>
#include <cstring>
#include <fstream>
#include "FuncLibrary.h"
#include "Node.h"

using namespace std;

template < typename T >
class TasksList
{
private:
	Node<T> *head, *tail;
public:
	TasksList();

	Node<T>* getHead() const;
	Node<T>* getTail() const;
	void setHead(Node<T>* ptr);
	void setTail(Node<T>* ptr);
	void addToHead(T item);
	void addToTail(T item);
	void addToIndex(T item, int index);//index 1 means 1
	void addInOrder(T item);
	T removeHead();
	T removeTail();
	T removeFromIndex(int i);//index 1 means 0
	T removeItem(char item[]);

	void removeWithPredicate(bool(*predicate)(T item1, char data[]), char item[]);

	void sort(bool (*sortCriteria)(T item1, T item2), bool order=0); //using insertion sort
                                                                     //order: 0-> ascending 1->descending
    void merge(TasksList<T> anotherList);
    bool search(T item);
    void reverse();
	void display(int index=0);
	int length();
	bool is_empty(std::ifstream& pFile);
	template <class TT>
	friend ostream& operator<<(ostream&, TasksList<TT>);
	void save();
	void clear();
	~TasksList();
};
#include "TasksList.cpp"
#endif
